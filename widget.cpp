#include "widget.h"
#include "ui_widget.h"
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    setAvaliable();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::setAvaliable()
{
    if (this->money >= COFFEE)
        ui->pb_coffee->setEnabled(true);
    else
        ui->pb_coffee->setDisabled(true);

    if (this->money >= TEA)
        ui->pb_tea->setEnabled(true);
    else
        ui->pb_tea->setDisabled(true);

    if (this->money >= MILK)
        ui->pb_milk->setEnabled(true);
    else
        ui->pb_milk->setDisabled(true);


    ui->lcdNumber->display(this->money);
}
void Widget::insertMoney(int money)
{
    this->money += money;
    setAvaliable();
}

void Widget::dispenseItem(Item item)
{
    auto diff = this->money - item;

    if (diff < 0) {
        return;
    }

    this->money = diff;
    setAvaliable();
}

void Widget::on_pb_10_clicked()
{
    this->insertMoney(10);
}

void Widget::on_pb_50_clicked()
{
    this->insertMoney(50);
}

void Widget::on_pb_100_clicked()
{
    this->insertMoney(100);
}

void Widget::on_pb_500_clicked()
{
    this->insertMoney(500);
}

void Widget::on_pb_coffee_clicked()
{
    this->dispenseItem(COFFEE);
}

void Widget::on_pb_tea_clicked()
{
    this->dispenseItem(TEA);
}

void Widget::on_pb_milk_clicked()
{
    this->dispenseItem(MILK);
}

void Widget::on_pb_reset_clicked()
{
    this->money = 0;
    setAvaliable();
    this->m.information(nullptr, "RESET", "awesome mentor gilgil");
}
